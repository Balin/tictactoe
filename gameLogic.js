var isXTurn = true;
var hasMatchEnded = false;
var gameState = [[]]
var holder = document.getElementById('elemHolder');
var winnerString = document.getElementById('winnerString');
var resetButton = document.getElementById('resetButton');
var addElement = "";
var isTied = 0;

function SetupGame(){
    gameState = [[]];
    isXTurn = true;
    hasMatchEnded = false;
    addElement = "";
    isTied = 0;

    for(var i=0; i<3; i++){
      gameState.push([]);
      addElement += '<tr>';
      for(var j=0; j<3; j++){
        gameState[i].push('-');
        addElement += '<td id="elem'+i+j+'" class="w3-hover-deep-purple w3-topbar w3-leftbar w3-bottombar w3-rightbar w3-jumbo h1" onclick="GameAction('+i+','+j+')" style="width:140px"><strong>-</strong></td>';
      }
      addElement += '</tr>';
    }
    holder.innerHTML = addElement;
    winnerString.innerHTML = (isXTurn? 'X':'O')+' turn!';
}

function GameAction(x,y){
  //console.log(x+'/'+y);
  if(hasMatchEnded){   return; }
  if(!myTurn){	return;	}
  if(gameState[x][y] != '-'){  return; }
  var newElement = (isXTurn? 'X':'O');
  isXTurn = !isXTurn;
  winnerString.innerHTML = (isXTurn? 'X':'O')+' turn!';
  GameMessage(x,y);
  SetSquare(x,y, newElement);
  isTied++;
  var winner = CheckWinner();
  if(winner == 'X' || winner == 'O' || isTied == 9){
	 if(winner == undefined){   winnerString.innerHTML = 'Capivara!'; }
	 else{ winnerString.innerHTML = winner+' has Won!';  }
	 resetButton.style.display = 'initial';
	 hasMatchEnded = true;
  }
}

function SetSquare(x,y, element){
   document.getElementById('elem'+x+''+y).innerHTML = '<strong>'+element+'</strong>';
   gameState[x][y] = element;
}

function CheckWinner(){
  var hasOWon = false;
  var hasXWon = false;

  var xCounter = 0;
  var oCounter = 0;

  //console.log('========= Checking Winner =========');
  for(var x=0; x<3; x++){
    xCounter = 0;
    oCounter = 0;
    //console.log('Column: '+x);
    for(var y=0; y<3; y++){
      //console.log('Row: '+y);
      if(gameState[x][y] == 'X'){
        xCounter++;
        //console.log(x+'|'+y+':X');
        if(xCounter == 3){   return 'X'; }
      }
      if(gameState[x][y] == 'O'){
        oCounter++;
        //console.log(x+'|'+y+':O');
        if(oCounter == 3){   return 'O'; }
      }
      //if(gameState[x][y] == '-'){  console.log(x+'|'+y+':-'); }
    }
    //console.log('===============');
  }
  for(var y=0; y<3; y++){
    xCounter = 0;
    oCounter = 0;
    //console.log('Row: '+y);
    for(var x=0; x<3; x++){
      if(gameState[x][y] == 'X'){
        xCounter++;
        //console.log(x+'|'+y+':X');
        if(xCounter == 3){   return 'X'; }
      }
      if(gameState[x][y] == 'O'){
        oCounter++;
        //console.log(x+'|'+y+':O');
        if(oCounter == 3){   return 'O'; }
      }
      //if(gameState[x][y] == '-'){  console.log(x+'|'+y+':-'); }
    }
    //console.log('===============');
  }
  {
    var x = 0;
    var y = 0;
    xCounter = 0;
    oCounter = 0;
    //console.log('Primary Diagonal');
    while (x < 3) {
      if(gameState[x][y] == 'X'){
        xCounter++;
        //console.log(x+'|'+y+':X');
        if(xCounter == 3){   return 'X'; }
      }
      if(gameState[x][y] == 'O'){
        oCounter++;
        //console.log(x+'|'+y+':O');
        if(oCounter == 3){   return 'O'; }
      }
      //if(gameState[x][y] == '-'){  console.log(x+'|'+y+':-'); }
      x++;
      y++;
    }
    //console.log('===============');
  }
  {
    var x = 2;
    var y = 0;
    xCounter = 0;
    oCounter = 0;
    //console.log('Secondary Diagonal');
    while (x >= 0) {
      if(gameState[x][y] == 'X'){
        xCounter++;
        //console.log(x+'|'+y+':X');
        if(xCounter == 3){   return 'X'; }
      }
      if(gameState[x][y] == 'O'){
        oCounter++;
        //console.log(x+'|'+y+':O');
        if(oCounter == 3){   return 'O'; }
      }
      //if(gameState[x][y] == '-'){  console.log(x+'|'+y+':-'); }
      x--;
      y++;
    }
    //console.log('===============');
  }
}
