const eventEmitter = require('events');
const datagram = require('dgram');
const os = require('os');

var localIPs = [];
var socket;
var gamePort = 9001;

var avaliableGames = [];
var inviter = -1;
var inviting = false;
var inGame = false;
var gameMessageTimeout;
var lastPosition = -1;
var opponentIP;
var myTurn = false;
var lastMessage = -1;
class NetworkEventEmitter extends eventEmitter{}
const NetworkEmitter = new NetworkEventEmitter();

function StartListening(){
	socket = datagram.createSocket('udp4');
	socket.bind({address: '0.0.0.0', port: gamePort, exclusive: true}, (bindError) =>{
   	socket.setBroadcast(true);
	});

	var ifaces = os.networkInterfaces();
	Object.keys(ifaces).forEach(function (ifname){
		ifaces[ifname].forEach(function (iface){
			var alias = 0;
			if('IPv4' !== iface.family || iface.internal !== false){	return;	}

			if(alias >= 1){	localIPs.push(iface.address);	}
			else{	localIPs.push(iface.address);	}
			alias++;
		});
	});

	//gameFound = new Event('gameFound');

	socket.on('error', (errorMsg) =>{
   	console.log(`Socket error: ${errorMsg.stack}`);
		socket.close();
	});

	socket.on('message', (msg, info) =>{
   	console.log(`Received: ${msg} from ${info.address}:${info.port}`);

		var isMyPacket = false;
		localIPs.forEach(function(localIP){
			if(info.address == localIP){	isMyPacket = true;	}
		});
		if(isMyPacket){
			console.log('My own packet!');
			return;
		}
		var messagePosition = 0;
		var messageType = msg.readInt8(messagePosition);
		console.log(messageType);
		switch (messageType) {
			case 0x44:
				//Received Game discovery request
				console.log('Game discovery request!');
				if(inGame){	break;	}
				SendPlayerInfo(info.address);
				break;
			case 0x45:
				//Received Game info
				messagePosition++;
				console.log('Received new game');

				var remotePlayerNameLenght = msg.readInt8(messagePosition);
				messagePosition++;
				var remotePlayerName = msg.toString('utf8', messagePosition, messagePosition+remotePlayerNameLenght);
				messagePosition += remotePlayerNameLenght;
				console.log(remotePlayerName);
				console.log(remotePlayerNameLenght);

				var remotePlayerQuestLenght = msg.readInt8(messagePosition);
				messagePosition++;
				var remotePlayerQuest = msg.toString('utf8', messagePosition,messagePosition+remotePlayerQuestLenght);
				messagePosition += remotePlayerQuestLenght;
				console.log(remotePlayerQuest);
				console.log(remotePlayerQuestLenght);

				var remotePlayerColorLenght = msg.readInt8(messagePosition);
				messagePosition++;
				var remotePlayerColor = msg.toString('utf8', messagePosition,messagePosition+remotePlayerColorLenght);
				messagePosition += remotePlayerColorLenght;
				console.log(remotePlayerColor);
				console.log(remotePlayerColorLenght);

				var newGame = [remotePlayerName, remotePlayerQuest, remotePlayerColor, info.address];
				//Checks if game is already listed
				var i = 0;
				for(var key in avaliableGames){
					if(avaliableGames.hasOwnProperty(key)){
						console.log(info);
						console.log(avaliableGames[key]);
						if(avaliableGames[key][3] == info.address){	cosole.log('aaaaaaaaaaa');	return;	}
						i++;
					}
				}
				console.log(avaliableGames.push(newGame));
				console.log(newGame);
				NetworkEmitter.emit('GameFound');
				break;
			case 0x46:
				//Received game invite
				//alert(`IP Address ${info.address} invited you to a game`);
				if(inviter != -1 || inviting || inGame){	break;	}
				invitedModal = document.getElementById('invitedModal');
				inviterName = document.getElementById('inviterName');
				var i = 0;
				for(var key in avaliableGames){
					if(avaliableGames.hasOwnProperty(key)){
						if(avaliableGames[key][3] == info.address){	break;	}
						i++;
					}
				}
				inviterName.innerHTML = avaliableGames[i][0];
				inviter = i;
				invitedModal.style.display = 'block';
				break;
			case 0x47:
				if(!inviting || inGame){	break;	}
				inviting = false;
				//inviteWaitModal = document.getElementById('inviteWaitModal');
				//inviteWaitModal.style.display = 'none';
				opponentIP = info.address;
				InviteAnswer(true);
				break;
			case 0x48:
				if(!inviting || inGame){	break;	}
				inviting = false;
				//inviteWaitModal = document.getElementById('inviteWaitModal');
				//inviteWaitModal.style.display = 'none';
				InviteAnswer(false);
				break;
			case 0x49:
				if(inGame){	break;	}
				SetupGame();
				document.getElementById('networkScreen').style.display = 'none';
				document.getElementById('gameScreen').style.display = 'block';
				opponentIP = info.address;
				inGame = true;
				myTurn = false;
				break;
			case 0x4A:
				console.log(`${msg[0]} | ${msg[1]}`);
				if(!inGame){	break;	}
				messagePosition++;
				var playedPosition = msg.readUInt8(messagePosition);
				var x;	var y;
				for(var i=0; i<3; i++){	for(var j=0; j<3; j++){	if((i*3+j)==playedPosition){	x = i;	y = j;	}	}	}
				SetSquare(x,y,(isXTurn? 'X':'O'));
				isTied++;
				isXTurn = !isXTurn;
				winnerString.innerHTML = (isXTurn? 'X':'O')+' turn!';
				SendAck();
				myTurn = true;
				var winner = CheckWinner();
			   if(winner == 'X' || winner == 'O' || isTied == 9){
			 		if(winner == undefined){   winnerString.innerHTML = 'Capivara!'; }
			 		else{ winnerString.innerHTML = winner+' has Won!';  }
			 		resetButton.style.display = 'initial';
			 		hasMatchEnded = true;
			   }
				break;
			case 0x4B:
				console.log('Received acknowlege');
				if(!inGame){	break;	}
				clearTimeout(gameMessageTimeout);
				break;
			default:

		}
	});
}

function ResetGame(){
	resetButton.style.display = 'none';
	inGame = false;
	document.getElementById('networkScreen').style.display = 'block';
	document.getElementById('gameScreen').style.display = 'none';
}

function SendAck(){
	var ack = Buffer.alloc(1);
	ack.writeUInt8(0x4B,0);
	console.log(`Sending: ${ack[0]}`);
	socket.send(ack, gamePort, opponentIP, (sendError) => {
		console.log(`Socket error: ${sendError.stack}`);
	});
}

function RevertTurn(){
	myTurn = true;
	var x;
	var y;
	for(var i=0; i<3; i++){
		for(var j=0; j<3; j++){
			if((i*3+j)==lastPosition){
				x = i;
				y = j;
			}
		}
	}
	SetSquare(x,y,'-');
	isTied--;
	isXTurn = !isXTurn;
	hasMatchEnded = false;
}

function GameMessage(x,y){
	var gameBuffer = Buffer.alloc(2);
	gameBuffer.writeUInt8(0x4A,0);
	var position = x*3+y;
	lastPosition = position;
	gameBuffer.writeUInt8(position,1);
	socket.send(gameBuffer, gamePort, opponentIP, (sendError) => {
		console.log(`Socket error: ${sendError.stack}`);
	});
	gameMessageTimeout = setTimeout(RevertTurn,1500);
	myTurn = false;
}

function ClearGames(){
	avaliableGames = [];
}

function GameStart(){
	SetupGame();
	var startGameBuffer = Buffer.alloc(1);
	startGameBuffer.writeUInt8(0x49);
	socket.send(startGameBuffer, gamePort, opponentIP, (sendError) => {
		console.log(`Socket error: ${sendError.stack}`);
	});
	document.getElementById('networkScreen').style.display = 'none';
	document.getElementById('gameScreen').style.display = 'block';

	inGame = true;
	myTurn = true;
}

function Send(sendString){
	console.log(`Sending: ${sendString}`);
	socket.send(sendString, gamePort, '192.168.1.255', (sendError) => {
   	console.log(`Socket error: ${sendError.stack}`);
	});
}

function SendLanDiscovery(){
	//Message Format:
	//(byte)		Message Type
	var discoveryMessage = Buffer.from([0x44]);
	console.log(`Sending game discovery message ${discoveryMessage}`);
	socket.send(discoveryMessage, gamePort, '192.168.1.255', (sendError) =>{
		console.log(`socket error: ${sendError.stack}`);
	});
}

function SendPlayerInfo(requestAddress){
	//Message Format:
	//(byte)		Message type
	//(byte)		Player name lenght -> X
	//X(byte)	Player name
	//(byte)		Player quest lenght -> Y
	//Y(byte)	Player quest
	//(byte)		Player color lenght -> Z
	//Z(byte)	Player color
	var sendBufferPosition = 0;
	var pNameLength = Buffer.byteLength(playerName, 'utf8');
	console.log(pNameLength);
	console.log(Buffer.from(playerName));
	var pQuestLength = Buffer.byteLength(playerQuest, 'utf8');
	console.log(pQuestLength);
	console.log(Buffer.from(playerQuest));
	var pColorLength = Buffer.byteLength(playerColor, 'utf8');
	console.log(pColorLength);
	console.log(Buffer.from(playerColor));
	var sendBuffer = Buffer.alloc(4+pNameLength+pQuestLength+pColorLength);
	console.log(sendBuffer.length);

	sendBufferPosition = sendBuffer.writeUInt8(0x45, sendBufferPosition);
	console.log(sendBufferPosition);

	sendBufferPosition = sendBuffer.writeUInt8(pNameLength, sendBufferPosition);
	sendBufferPosition += sendBuffer.write(playerName, sendBufferPosition);
	console.log(sendBufferPosition);

	sendBufferPosition = sendBuffer.writeUInt8(pQuestLength, sendBufferPosition);
	sendBufferPosition += sendBuffer.write(playerQuest, sendBufferPosition);
	console.log(sendBufferPosition);

	sendBufferPosition = sendBuffer.writeUInt8(pColorLength, sendBufferPosition);
	sendBufferPosition += sendBuffer.write(playerColor, sendBufferPosition);
	console.log(sendBufferPosition);

	console.log(sendBuffer);
	console.log(`Sending player info to ${requestAddress}`);
	socket.send(sendBuffer, gamePort, /*'192.168.1.255'*/requestAddress, (sendError) =>{
		console.log(`socket error: ${sendError.stack}`);
	});
}

function InviteGame(rowNumber){
	inviting = true;
	inviteMessage = document.getElementById('inviteMessage');
	inviteMessage.innerHTML = 'Waiting for invite response...';
	inviteWaitModal = document.getElementById('inviteWaitModal');
	inviteWaitModal.style.display = 'block';
	SendGameInvite(avaliableGames[rowNumber][3]);
}

function SendGameInvite(inviteAddress){
	console.log(`Sending Game Invite to ${inviteAddress}`);
	var inviteBuffer = Buffer.alloc(1);
	inviteBuffer.writeUInt8(0x46);
	socket.send( inviteBuffer, gamePort, inviteAddress, (inviteError) => {
		console.log(`socket error: ${inviteError.stack}`);
	});
}

function CancelInvite(){
	inviteWaitModal = document.getElementById('inviteWaitModal');
	inviteWaitModal.style.display = 'none';
	inviting = false;
}

function InviteResponse(isAccepted){
	var responseBuffer = Buffer.alloc(1);
	if(isAccepted){	responseBuffer.writeUInt8(0x47);	console.log('Accepted Game');	}
	else{					responseBuffer.writeUInt8(0x48);	console.log('Rejected Game');	}

	invitedModal = document.getElementById('invitedModal');
	invitedModal.style.display = 'none';

	console.log(`Invite response to ${avaliableGames[inviter][3]} ${isAccepted}`);
	socket.send( responseBuffer, gamePort, avaliableGames[inviter][3], (inviteError) => {
		console.log(`socket error: ${inviteError.stack}`);
	});
	inviter = -1;
}

function InviteAnswer(accepted){
	inviteMessage = document.getElementById('inviteMessage');
	inviteMessage.innerHTML = (accepted? 'Player accepted! :)':'Player rejected! :(');
	setTimeout(closeModal,2000);
	if(accepted){
		GameStart();
	}
}

function closeModal(){
	inviteWaitModal = document.getElementById('inviteWaitModal');
	inviteWaitModal.style.display = 'none';
	inviting = false;
}
